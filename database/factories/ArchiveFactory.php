<?php

use Faker\Generator as Faker;
use Modules\Archiving\Entities\Archive;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Archive::class, function (Faker $faker) {
    /* `watheqdev`.`archiving__archives` */
    $archiving__archives =
        [
            //'id' => '285',
            'barcode' => '1555951781','doc_no' => '1440-311-1-285',
            'folder_id' => ["311"],
            'description' => 'ارشفة','file_type_id' => '1','start_date' => '2019-07-11 17:14:43','start_date_hijri' => '1440-11-08 17:14:43',
            'end_date' => '2019-07-12 09:45:22','end_date_hijri' => '1440-11-09 09:45:22',
            'assign_to' => '168','user_id' => '1','department_id' => '108','benfeciary_id' => '13',
            'files' => NULL,'is_orc' => '0','is_orc_read' => '0','orc_data' => NULL,
            'priority_id' => '1','value' => '1000.00','currency_id' => '3',
            'deleted_at' => NULL,'created_at' => '2019-07-11 16:14:43',
            'updated_at' => '2019-07-11 16:14:43'

    ];
    return $archiving__archives;
});
