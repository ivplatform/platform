<?php



use Faker\Generator as Faker;

use Modules\Importexport\Entities\Transaction;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Transaction::class, function (Faker $faker) {

    $im_ex__transactions = array(
        //'id' => '198',
        'user_id' => '1','department_id' => '108',
        'transaction_type_id' => '1','operation_type_id' => '1',
        'operation_privacy_id' => '1','importance_level_id' => '1','bar_code' => '1530803017',
        'operation_subject' => 'معاملة 1','doc_no' => '1440/198/1',
        'doc_number' => '1','operation_delivery_id' => '4',
        'milady_date' => '2019-07-11 03:15:49','higry_date' => '1440-11-08 03:15:49',
        'related_transaction_id' => '0','benfeciaries_id' => '14','activate_ocr' => '0',
        'created_at' => '2019-07-11 16:29:53','updated_at' => '2019-07-11 16:29:53'
    );
    return $im_ex__transactions;
});
