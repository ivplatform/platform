<?php

use Faker\Generator as Faker;
use Modules\Tasking\Entities\Task;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Task::class, function (Faker $faker) {
    /* `watheqdev`.`tasking__tasks` */
    $tasking__tasks = [
            //'id' => '101',
            'assign_to' => '167',
            'title' => 'مهمة جديدة',
            'message' => '<p>مهمة جديدة&nbsp;</p>',
            'files' => NULL,'time' => '2019-07-11 16:20:31',
            'endtime' => '2019-07-13 11:00:21',
            'endtimeHijri' => '1440-11-10 11:00:21',
            'priority_id' => '2','user_id' => '1',
            'status' => '1','complete' => '0',
            'deleted_at' => NULL,
            'created_at' => '2019-07-11 16:20:31',
            'updated_at' => '2019-07-11 16:20:31'
    ];
    return $tasking__tasks;
});
