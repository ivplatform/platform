<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\Archiving\Entities\Archive;
use Carbon\Carbon;
use Modules\Notify\Contract\Notification;
use Illuminate\Support\Facades\Log;

class ArchiveEndDateOver extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'endDateOver:archive';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send Notification for user before expire date of archive end date!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    protected $notify;

    public function __construct(Notification $notify) {
        parent::__construct();
        $this->notify = $notify;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        // calculate new statistics
        $sendToUser = [];

        $archives = Archive::where('end_date',  '>=', Carbon::today()->addDays(0))
            ->where('end_date', '<', Carbon::today()->addDays(1))
            ->get();

        // update statistics table
        // send notification for each user that acrhive has assigned to him/her
        foreach ($archives as $archive) {
            // Log::info('end date: '.$archive->end_date. ' date tomorrow:'. Carbon::today());;
            /////////////////////////////////////////////////
            // send notification for user created archive and  each user has been assign to acchive to tell expire date
            $sendToUser = [$archive->user_id,$archive->assign_to];
            $jsonBody=json_encode(['number'=>$archive->id]);///
            $this->notify->push(
                "endDateOver_title" ,
                "endDateOver_body$#$$jsonBody",
                'fas fa-file-archive',
                $sendToUser,
                'dashboard/archiving/show/'.$archive->id, //route('admin.archiving.archive.show', [$archive->id])
                'archiving'
            );

            /////////////////////////////////////////////////

        }
    }

}
