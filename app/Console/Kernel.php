<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\ArchiveExpireEndDate',
        'App\Console\Commands\TaskEndDateOver',
        'App\Console\Commands\TaskExpireEndDate',
        'App\Console\Commands\Backup',
        'App\Console\Commands\AssignmentsExpireAndAboutTo',
        'App\Console\Commands\ArchiveEndDateOver',
        'App\Console\Commands\RestoreDemo',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        $schedule->command('expireEndDate:archive')->daily();
        $schedule->command('expireEndDate:task')->daily();
        $schedule->command('endDateOver:task')->daily();

        try {
            $BackupType = \Setting::get('backup::Backup-Type');

            if (in_array($BackupType,["hourly","weekly","monthly","daily","everyMinute"])){
                $schedule->command('mybackup:run')->$BackupType();
            }
        }
        catch(\Illuminate\Database\QueryException $ex){
            \Log::info("Error In DB While Getting Backup Settings");
            \Log::info($ex);

        }
        $schedule->command('assignment:expire')->everyMinute();
        $schedule->command('endDateOver:archive')->daily();

        // restore demo.sql for customer every day demo
        $trial=env("TRIAL", false);
        if($trial){
            $schedule->command('restore:demo')->daily();
        }


    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
