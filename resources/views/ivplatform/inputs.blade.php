
{!! Form::label("{$lang}[title]", trans('blog::post.form.title')) !!}
{!! Form::text("{$lang}[title]", old("$lang.title"), ['class' => 'form-control', 'data-slug' => 'source', 'placeholder' => trans('blog::post.form.title')]) !!}


{!! Form::label("{$lang}[title]", trans('news::news.news.title')) !!}
{!! Form::i18nInput("title", "{$lang}[title]", $errors,$lang) !!}
{!! Form::i18nTextarea("body", "{$lang}[body]", $errors,$lang) !!}

<!-- edit example -->
{!! Form::label("{$lang}[title]", trans('news::news.news.title')) !!}
{!! Form::i18nInput("title", "{$lang}[title]", $errors,$lang,$news) !!}
{!! Form::i18nTextarea("body", "{$lang}[body]", $errors,$lang,$news) !!}
